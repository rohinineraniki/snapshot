import React from "react";
import "./style.css";

class Category extends React.Component {
  render() {
    let { searchTypes } = this.props;
    let renderCatagories = searchTypes.map((each) => {
      return (
        <button key={each} onClick={this.props.tagChange} className="button">
          {each}
        </button>
      );
    });
    return <div className="button-container">{renderCatagories}</div>;
  }
}

export default Category;
