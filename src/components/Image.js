import React from "react";
import "./style.css";

class Image extends React.Component {
  render() {
    return (
      <li>
        <img src={this.props.photo} alt="" className="image" />
      </li>
    );
  }
}

export default Image;
