import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";

import Image from "./Image";
import Loader from "./Loader";
import { Navigate } from "react-router-dom";
import Category from "./Category";

import "./style.css";

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      search: "",
      invalidSearch: null,
      photos: [],
      noPhotoFound: null,
      searchedImage: null,
      isLoading: false,
      apiError: false,
      results: [],
    };

    this.flickr_key = "555ef53e70b4b76200505bd8c13f1fd1";
  }

  setIsLoading(value) {
    this.setState({
      isLoading: value,
    });
  }

  onInputChange = (event) => {
    this.setState({
      search: event.target.value,
    });
  };

  onCatagoryChange = (event) => {
    this.setState(
      {
        search: event.target.innerText,
      },
      this.doSearch
    );
  };

  handleSubmit = (event) => {
    event.preventDefault();

    if (this.state.search.trim().length === 0) {
      this.setState({
        invalidSearch:
          "Please enter in Searchbox above or choose any one from below catagories",
        photos: [],
        searchedImage: null,
        isLoading: false,
      });
    } else {
      this.doSearch();
    }
  };

  doSearch = () => {
    this.setIsLoading(true);
    fetch(
      `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${
        this.flickr_key
      }&tags=${this.state.search.trim()}&per_page=24&format=json&nojsoncallback=1`
    )
      .then((response) => {
        if (response.ok) {
          this.setState({
            apiError: false,
          });

          return response.json();
        } else {
          console.log("Not successful fetch");
          this.setIsLoading(false);
          this.setState({
            apiError: true,
          });
        }
      })
      .then((data) => {
        if (data.photos.photo.length === 0) {
          this.setState({
            invalidSearch: null,
            noPhotoFound:
              "Images you searched for not found!... please search something else",
            photos: [],
            searchedImage: null,
            isLoading: false,
          });
        } else {
          let photos = [];
          let results = [];
          photos = data.photos.photo.map((image) => {
            let url = `https://live.staticflickr.com/${image.server}/${image.id}_${image.secret}_q.jpg`;

            return url;
          });
          results = photos.map((photo) => {
            return <Image key={photo} photo={photo} alt="image" />;
          });
          this.setState({
            invalidSearch: null,
            noPhotoFound: null,
            photos: photos,
            searchedImage: this.state.search + " images",
            results: results,
          });

          this.setIsLoading(false);
        }
      })
      .catch((error) => {
        // navigate to error page
        this.setIsLoading(false);
        this.setState({
          apiError: true,
        });
      });
  };

  render() {
    let searchTypes = ["Mountain", "Beaches", "Birds", "Food"];
    let { results } = this.state;
    console.log(results);
    return (
      <React.Fragment>
        <header>
          <h1>SnapShot</h1>
        </header>

        {this.state.apiError ? <Navigate to="/error" replace={true} /> : null}

        <div className="bg-container">
          <form onSubmit={this.handleSubmit} className="search-form">
            <input
              type="text"
              placeholder="Search..."
              onChange={this.onInputChange}
              value={this.state.search}
            ></input>

            <button type="submit" className="submit-button">
              <FontAwesomeIcon
                icon={faMagnifyingGlass}
                className="search-icon"
              />
            </button>
          </form>
        </div>

        <p>{this.state.invalidSearch}</p>

        <Category searchTypes={searchTypes} tagChange={this.onCatagoryChange} />

        <h1 className="not-found">{this.state.noPhotoFound}</h1>

        {this.state.isLoading ? (
          <Loader />
        ) : (
          <>
            <h1 className="searched-image">{this.state.searchedImage}</h1>

            <ul className="image-gallery">{results}</ul>
          </>
        )}
      </React.Fragment>
    );
  }
}

export default Search;
