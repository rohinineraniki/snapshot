import React from "react";
import { Link } from "react-router-dom";

class ShowError extends React.Component {
  render() {
    return (
      <>
        {
          <Link to="/">
            <header>
              <h1 className="heading">Snap Shot</h1>
            </header>
          </Link>
        }
        <h1>Something went wrong</h1>
        <h2>Sorry, we can't find the page you are looking for</h2>
        <Link to="/">
          <div className="error-button-container">
            <button className="error-button">Go to home</button>
          </div>
        </Link>
      </>
    );
  }
}

export default ShowError;
